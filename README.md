# objc4-818.2

#### 介绍
objc4-818.2 源码编译

#### 编译环境

macOS Big Sur 版本11.2.3 
MacBook Pro (13-inch, M1, 2020)
芯片 Apple M1

Xcode Version 12.4 (12D4e)

#### 安装教程

1.  参考1：[https://www.jianshu.com/p/e2791e95555b](https://www.jianshu.com/p/e2791e95555b)
2.  参考1：[https://www.jianshu.com/p/9e0fc8295c4b](https://www.jianshu.com/p/9e0fc8295c4b)

#### 使用说明
